angular
  .module('models', [])

  .factory('User', function() {
    function User(userData) {
      this.setData(userData);
    };

    User.prototype = {
      setData: function(userData) {
        angular.extend(this, userData)
      },
      update: function(userData) {
        this.setData(userData);
      }
    };

    return User;
  })

  .factory('HttpBackendMock', function() {
    function HttpBackendMock() {};
    HttpBackendMock.prototype = {
      post: function(url, data) {
        return new Promise(function(resolve, reject) {
          setTimeout(function() {
            var randNum = Math.random();
            if (randNum > 0.0 && randNum <= 0.33) {
              resolve({
                status: 200,
                data: data,
              });
            } else if (randNum > 0.33 && randNum <= 0.66) {
              reject({
                status: 400,
                data: {
                  invalidFields: {
                    'name': 'Already taken. Please, choose another one',
                  }
                }
              });
            } else {
              reject({
                status: 500,
                data: {},
              });
            }
          }, 100);
        })
      }
    }
    return new HttpBackendMock();
  })

  .factory('UserService', [
    'User', 'ServerError', 'HttpBackendMock',
    function(User, ServerError, HttpBackendMock) {
      function UserService() {
        this.apiBase = 'http://api.backend.com/users';
      };

      UserService.prototype = {
        createUser: function(userData) {
          return new Promise(function(resolve, reject) {
            HttpBackendMock.post(this.apiBase, userData)
              .then(
                function(response) {
                  resolve(new User(response.data));
                },
                function(response) {
                  reject(new ServerError(response));
                }
              )
          });
        }
      }
      return new UserService();
    }
  ])

  .factory('ServerError', function() {
    function ServerError(response) {
      angular.merge(this, response);
    }

    ServerError.prototype = {
      isInternalError: function() {
        return this.status === 500;
      },

      isValidationError: function() {
        return this.status === 400;
      },

      getInvalidFields: function() {
        return this.data.invalidFields;
      },
    }
    return ServerError;
  })
