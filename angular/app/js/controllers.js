angular
  .module('controllers', ['models'])
  .controller(
    'UserFormController',
    ['$scope', '$parse', 'User', 'UserService', 'ServerError',
      function($scope, $parse, User, UserService, ServerError) {
        $scope.userData = {
          gender: 'male',
        };

        $scope.users = [];

        $scope.clearErr = function(field) {
          var serverMessage = $parse('form.'+field+'.$error.serverMessage');
          $scope.form.$setValidity(field, true, $scope.form);
          serverMessage.assign($scope, undefined);
        }

        $scope.createUser = function(userData) {
          $scope.serverError = undefined;

          UserService.createUser(userData)
            .then(function(newUser) {
              $scope.$apply(function() {
                console.log("New user");
                $scope.users.push(newUser);
              })
            }, function(serverError) {
              if (serverError.isInternalError()) {
                console.log("Server error");
                $scope.$apply(function() {
                  $scope.serverError = serverError;
                })
              } else if (serverError.isValidationError()) {
                console.log("Validation error")
                var invFields = serverError.getInvalidFields();
                for (var field in invFields) {
                  $scope.$apply(function() {
                    var serverMessage = $parse('form.'+field+'.$error.serverMessage');
                    $scope.form.$setValidity(field, false, $scope.form);
                    serverMessage.assign($scope, invFields[field]);
                  })
                }
              } else {
                console.log("Unknown error");
              }
            })
        }
      }
    ]
  );