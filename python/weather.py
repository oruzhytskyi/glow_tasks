from datetime import date
from requests import get


def summarize_forecast(city_name):
    forecast_data = get_forecast(city_name)
    summary = {
        'city': city_name,
        'max': forecast_data['list'][0]['temp']['max'],
        'min': forecast_data['list'][0]['temp']['min'],
        'forecasts': {},
    }

    for day_forecast in forecast_data['list']:
        summary['max'] = max(summary['max'], day_forecast['temp']['max'])
        summary['min'] = min(summary['min'], day_forecast['temp']['min'])
        forecast_date = date.fromtimestamp(day_forecast['dt']).isoformat()
        summary['forecasts'].setdefault(
            day_forecast['weather'][0]['main'], []).append(forecast_date)

    return summary


def get_forecast(city_name):
    api_url = ('http://api.openweathermap.org/data/2.5/forecast/daily?'
               'q=%(city_name)s&units=imperial&cnt=14'
               '&appid=0edad35e873ccda85fc576ffff5a3f66') % {
                    'city_name': city_name
                }

    return get(api_url).json()
